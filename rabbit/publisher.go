/**
 * rabbitMQ 发布者
 * Created by 180215@gree.com on 13.十二月.2016
 */
package rabbit

import (
	"github.com/alecthomas/log4go"
	"github.com/streadway/amqp"
	"fmt"
	"RabbitDemo/conf"
)

type Data struct {
	Mac         string
	DataPackage string
}

type DataStorage struct {
	conn     *amqp.Connection
	channel  *amqp.Channel
	Queue    string
	QueueKey string
	Exch     string
	ExchType string
	PackChan chan *Data
}

type Listener struct {
	Param map[string]*DataStorage
}

var (
	dataStorage *DataStorage
	listener    *Listener
)

func GetListener() *Listener {

	if listener == nil {
		listener, err := InitPublisher()
		if err != nil {
			log4go.Error(err.Error())
			return nil
		}
		return listener
	}
	return listener
}

func InitPublisher() (*Listener, error) {
	tempMap := make(map[string]*DataStorage)

	for i := 0; i < conf.Int("rabbitmq.num"); i++ {

		index := fmt.Sprintf("%d", i)
		queue := conf.String("rabbitmq.queue." + index + ".name")
		routingKey := conf.String("rabbitmq.queue." + index + ".key")
		exch := conf.String("rabbitmq.exchange." + index + ".name")
		exchType := conf.String("rabbitmq.exchange." + index + ".type")

		dataStorage := &DataStorage{PackChan: make(chan *Data, 100)}

		err := dataStorage.connect(exch, exchType)
		if err != nil {
			log4go.Error("GetMQStorage connect %s error: %s", exch, err.Error())
			return nil, err
		}

		go dataStorage.DataCollStart(exch, exchType, queue, routingKey)

		tempMap[exch] = dataStorage
	}

	listener = &Listener{tempMap}

	return listener, nil
}

func (ds *DataStorage) DataCollStart(exchangeName, exchangeType, queue, routingKey string) {
	var dat *Data
	for {
		select {
		case dat = <-ds.PackChan:

			ds.publish(exchangeName, exchangeType, dat.DataPackage, routingKey+dat.Mac)
		}
	}
	ds.closeMQ()
}

func (ds *DataStorage) connect(exchangeName, excType string) error {
	var err error

	ds.conn, err = amqp.Dial(conf.String("rabbitmq.uri"))
	if err != nil {
		//noinspection ALL
		log4go.Error("Dialing amqp failed: %s", err.Error())
	}

	ds.channel, err = ds.conn.Channel()
	if err != nil {
		//noinspection ALL
		log4go.Error("Got channel failed: %s", err.Error())
	}

	if er := ds.channel.ExchangeDeclare(
		exchangeName, excType,
		true,  // durable
		false, // auto-delete
		false, // internal
		false, // no-wait
		nil,   // args
	); er != nil {
		//noinspection ALL
		log4go.Error("declaring %q Exchange (%q) failed: %s", excType, exchangeName, er.Error())
	}

	log4go.Debug("RabbitMQ connected. exchang:%s, exchType:%s", exchangeName, excType)

	return err
}

func (ds *DataStorage) closeMQ() {
	ds.channel.Close()
	ds.conn.Close()

}

//连接rabbitmq server
func (ds *DataStorage) publish(exchangeName, exchangeType, msgContent, key string) {

	log4go.Debug("Publishing -> exchange:%s, exchType:%s, routingKey:%s, msg:%s ", exchangeName, exchangeType, key, msgContent)
	if ds.channel == nil {
		// 检查channel
		ds.connect(exchangeName, exchangeType)
	}

	if err := ds.channel.Publish(
		exchangeName, key, false, false,
		amqp.Publishing{
			ContentType:  "text/plain",
			DeliveryMode: amqp.Persistent,
			Body:         []byte(msgContent),
		}); err != nil {
		log4go.Error("publish to exchange: %s error: (%s) %s.", exchangeName, key, err.Error())
	}
}
