package rabbit

import (
	"fmt"

	"github.com/streadway/amqp"
	"github.com/alecthomas/log4go"
	"time"
	"errors"
)

type Consumer struct {
	connection *amqp.Connection
	channel    *amqp.Channel
	tag        string
	done       chan error
}

func NewConsumer(conn *amqp.Connection, exchange, exchangeType, queueName, key, ctag string, f func(string, time.Time, []byte) error) (*Consumer, error) {

	var err error
	c := &Consumer{
		connection: conn,
		channel:    nil,
		tag:        ctag,
		done:       make(chan error),
	}

	//log4go.Debug("got Connection, getting Channel")
	c.channel, err = c.connection.Channel()
	if err != nil {
		log4go.Error(err.Error())
		return nil, fmt.Errorf("Channel: %s", err.Error())
	}

	//log4go.Debug("got Channel, declaring Exchange (%q)", exchange)
	if err := c.channel.ExchangeDeclare(
		exchange,     // name of the exchange
		exchangeType, // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return nil, fmt.Errorf("Exchange Declare: %s", err.Error())
	}

	//log4go.Debug("declared Exchange, declaring Queue %q", queueName)
	queue, err := c.channel.QueueDeclare(
		queueName, // name of the queue
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // noWait
		nil,       // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("Queue Declare: %s", err.Error())
	}

	//log4go.Debug("declared Queue (%q %d messages, %d consumers), binding to Exchange (key %q)",
	//	queue.Name, queue.Messages, queue.Consumers, key)
	if err := c.channel.QueueBind(
		queue.Name, // name of the queue
		key,        // bindingKey
		exchange,   // sourceExchange
		false,      // noWait
		nil,        // arguments
	); err != nil {
		return nil, fmt.Errorf("Queue Bind: %s", err.Error())
	}

	//log4go.Debug("Queue bound to Exchange, starting Consume (consumer tag %q)", c.tag)
	deliveries, err := c.channel.Consume(
		queue.Name, // name
		c.tag,      // consumerTag,
		false,      // noAck
		false,      // exclusive
		false,      // noLocal
		false,      // noWait
		nil,        // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("Queue Consume: %s", err)
	}

	go handle(deliveries, c.done, f)

	return c, nil
}

//noinspection ALL
func handle(deliveries <-chan amqp.Delivery, done chan error, f func(string, time.Time, []byte) error) {

	for d := range deliveries {

		err := f(d.Type, d.Timestamp, d.Body)
		if err != nil {
			log4go.Error(err.Error())
			if err != errors.New("bad packet") {
				//Ack掉错误的非未知包
				d.Ack(false)
			}
		} else {
			d.Ack(false)
		}

	}
	log4go.Error("handle: deliveries channel closed")
	done <- nil
}

func (c *Consumer) Shutdown() error {
	// 关闭 channel
	if err := c.channel.Cancel(c.tag, true); err != nil {
		log4go.Debug("Consumer cancel failed: %s", err.Error())
		return fmt.Errorf("consumer cancel failed: %s", err)
	}

	//关闭 rabbitmq 连接
	if err := c.connection.Close(); err != nil {
		log4go.Debug("AMQP connection close error: %s", err.Error())
		return fmt.Errorf("AMQP connection close error: %s", err)
	}

	defer log4go.Debug("AMQP shutdown OK")

	// 等待 handle() 退出
	return <-c.done
}
