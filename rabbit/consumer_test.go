package rabbit

import (
	"testing"
	"github.com/alecthomas/log4go"
	"github.com/streadway/amqp"
)

func TestNewConsumer(t *testing.T) {

	rbConn, err := amqp.Dial("amqp://test:test@localhost:5672/")
	if err != nil {
		log4go.Error(err.Error())
	}

	if _, err := NewConsumer(rbConn, "grih.fault", "direct", "grih.fault", "grih.fault.f0021ceac302", "tag", printer); err != nil {
		log4go.Error(err.Error())
	}
}


