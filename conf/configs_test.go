package conf

import (
	"reflect"
	"testing"
)

func TestStringList(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		// TODO: Add test cases.
		{
			name: "",
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StringList(tt.args.path); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StringList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLogEnv(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
		{
			name: "",
			args: args{},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LogEnv(tt.args.path); got != tt.want {
				t.Errorf("LogEnv() = %v, want %v", got, tt.want)
			}
		})
	}
}
