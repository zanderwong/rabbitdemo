package main

import (
	"sync"
	"RabbitDemo/rabbit"
	"github.com/alecthomas/log4go"
	"RabbitDemo/conf"
)

func main() {

	var wg sync.WaitGroup

	connSucessFlag := make(chan int, 1)

	rbConn, err := rabbit.GetMQConn(conf.String("rabbitmq.uri"), connSucessFlag)
	if err != nil {
		log4go.Error(err.Error())
	}
	wg.Add(1)

	go func() {
		for {
			<-rbConn.ConnFlag

			go consume(rbConn)
		}
	}()
	wg.Add(1)

	wg.Wait()

}

func consume(rbConn *rabbit.RbConn) {

	exchange := conf.String("rabbitmq.exchange.0.name")
	exchangeType := conf.String("rabbitmq.exchange.0.type")
	routingKey := conf.String("rabbitmq.queue.0.key")
	queue := conf.String("rabbitmq.queue.0.name")
	consumerTag := conf.String("rabbitmq.consumerTag")

	//initialize rabbitmq publisher
	rabbit.InitPublisher()
	log4go.Info("rabbitMQ publisher params initialize ok")
	//log4go.Debug("Consuming exchange:%s, type:%s, key:%s, queue:%s, ctag:%s", exchange, exchangeType, routingKey, queue, consumerTag)

	c, err := rabbit.NewConsumer(rbConn.RabbitConn, exchange, exchangeType, queue, routingKey, consumerTag, handlerData)
	if err != nil {
		log4go.Error(err.Error())
	}
	select {}

	log4go.Error("shutting down")

	if err := c.Shutdown(); err != nil {
		log4go.Error("error during shutdown: ", err)
	}
}